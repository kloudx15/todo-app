from django.urls import path
from accounts.views import (
    LoginLoginView,
    LogoutLogoutView,
    HomeView,
    signup,
)

# url path patterns here

urlpatterns = [
    path("login/", LoginLoginView.as_view(), name="login"),
    path("registration/logout/", LogoutLogoutView.as_view(), name="logout"),
    path("registration/signup/", signup, name="signup"),
    path("home/", HomeView.as_view(), name = "home")
]
