from django.shortcuts import redirect, render
from django.contrib.auth import login
from django.contrib.auth.views import LoginView, LogoutView
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.views.generic import TemplateView

# Create your views here.


class LoginLoginView(LoginView):
    template_name = "registration/login.html"


class LogoutLogoutView(LogoutView):
    template_name = "registration/logged_out.html"


def signup(request):
    if request.method == "POST":
        form = UserCreationForm(request.POST)
        if form.is_valid():
            username = request.POST.get("username")
            password = request.POST.get("password1")
            user = User.objects.create_user(
                username=username, password=password
            )
            user.save()
            login(request, user)
            return redirect("home")
    else:
        form = UserCreationForm(request.POST)
    context = {
        "form": form,
    }
    return render(request, "registration/signup.html", context)

class HomeView(TemplateView):
    template_name = 'home.html'