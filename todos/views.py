from django.urls import reverse_lazy
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.contrib.auth.mixins import LoginRequiredMixin
from todos.models import TodoItem, TodoList
# Create your views here.

class TodoListView(LoginRequiredMixin, ListView):
    model = TodoList
    template_name = 'pages/list.html'

    # def all_lists(request):

class TodoListDetailView(LoginRequiredMixin, DetailView):
    model = TodoList
    template_name = 'pages/detail.html'
    

class TodoListCreateView(LoginRequiredMixin, CreateView):
    model = TodoList
    template_name = 'pages/create.html'
    fields = ["name"]
    
    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.id])


class TodoListUpdateView(LoginRequiredMixin, UpdateView):
    model = TodoList
    template_name = 'pages/edit.html'
    fields = ["name"]
    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.id])

class TodoListDeleteView(LoginRequiredMixin, DeleteView):
    model = TodoList
    template_name = 'pages/delete.html'
    success_url = reverse_lazy("todo_list_list")

class TodoItemCreateView(LoginRequiredMixin, CreateView):
    model = TodoItem
    template_name = 'items/create.html'
    fields = ["task", "due_date", "is_completed", "list"]

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.list.id])

    def form_valid(self, form):
        return super().form_valid(form)

class TodoItemUpdateView(LoginRequiredMixin, UpdateView):
    model = TodoItem
    template_name = 'items/update.html'
    fields = ["task", "due_date", "is_completed", "list"]

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.list.id])
    
    def form_valid(self, form):
        return super().form_valid(form)
